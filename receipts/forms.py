from django.forms import forms, ModelForm
from receipts.models import Receipt, ExpenseCategory, Account

class NewReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = (
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        )

class NewCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = (
            "name",
        )

class NewAccountForm(ModelForm):
    class Meta:
        model = Account
        fields = (
            "name",
        )
